# Serwer Radius-alike
## Temat projektu
"Zrealizować system uwierzytelnienia użytkowników w oparciu o grupę serwerów protokołu RADIUS działających w hierarchii zaufania:
serwer nadrzędny + 2 serwery podrzędne.
Serwer podrzędny kieruje zapytanie do serwera nadrzędnego jeśli dany użytkownik nie występuje w lokalnej bazie danych.
Baza danych musi zawierać wpisy pozytywne i negatywne (uniemożliwiające uwierzytelnienie).
Dodatkowo zrealizować serwer proxy kierujący zapytania jednocześnie do 2 serwerów.
Wynik uwierzytelnienia w takim przypadku jest najgorszym wynikiem uwierzytelnienia z dwóch odpowiedzi."
## Opis projektu
Projekt zrealizowany w ramach przedmiotu Systemy Rozproszone. W implementacji wykorzystałem bibliotekę [eventmachine](http://rubyeventmachine.com) do zrealizowania serwera oraz bibliotekę [ruby-radius](https://github.com/nearbuy/ruby-radius) udostępniającą wrappery języka ruby na pakiety Radiusa.  

###Wymagania

* ruby 1.8.7
* [Bundler](http://gembundler.com "Bundler")  

###Instalacja
Należy przejść do folderu zawierającego pliki projektu i wykonać polecenie:

```
$ bundle install
```
## Opis użycia
###Serwer protokołu Radius
Serwer służy do odpowiadania na pakiety protokołu Radius zgodnie z opisem w dokumencie [RADIUS-alike protocol specification](https://docs.google.com/document/d/1GW-s8L0PKphUtIzk0JnOE5krszIxjigc308Fdnlp0Dg/edit#heading=h.vkgolmot2phs). Serwer może pracować w trybie nadrzędnym (major) oraz podrzędnym (minor). W trybie minor serwer kieruje zapytania do serwera major, jeżeli nie potrafi sam odpowiedzieć na pakiet typu Access-Request (użytkownik wymieniony w pakiecie nie znajduje się w bazie danych serwera minor).
Serwer uruchamia się poleceniem `$ radiusd.rb [OPCJE]` . Dostępne opcje to:

* -h - wyświetlenie pomocy
* -p[port] ustawia port nasłuchiwnania serwera (domyślnie **127.0.0.1**)
* -h[host] ustawia adres ip nasluchiwania serwera (domyślnie **1812**)
* -minor - serwer pracuje w trybie podrzędnym (domyślnie serwer pracuje jako major)
* -P[port] ustawia port serwera nadrzędnego (aktywne z flagą -minor), domyślnie **1812**
* -H[host] ustawia adres ip serwera nadrzednego (aktywne z flagą -minor), domyślnie **127.0.0.1**
* -f[plik] ładuje baze użytkowników z pliku (domyślnie baza użytkowników jest pusta)
* -i praca w trybie interaktywnym (odpowiedz po nacisnieciu ENTER)

#### Ustawienie hostu i portu nasłuchiwania
Opcja ta przydaje się, gdy potrzebna jest zmiana domyślnej konfiguracji. Przykłodowo, aby zmienić host na 192.167.100.101:1813 należy uruchomić serwer następującym poleceniem:

```
$ radius.rb -h192.167.100.101 -p1813
```
#### Ustawienie pracy serwera w trybie minor
W tym trybie serwer deleguje zapytanie do nadrzędnego serwera, jeżeli w zapytaniu znajduje się zapytanie o użytkownika, którego serwer nie ma w bazie danych. 
Konfigurację:

```
minor (127.0.0.1:1813) <---> (127.0.0.1:1812) major 
```

można uzyskać wpisując w jednym oknie terminala (**ważna jest kolejność uruchamiania serwerów**):

```
$ radiusd.rb #uruchomienie serwera major 
```

a w  drugim


```
$ radiusd.rb -minor -p1813 #uruchomienie serwera minor
```

Oczywiście można zmienić domyślne hosty/porty dla serwera minor/major, np:

```
minor (192.168.100.101:1814) <---> (192.168.100.100:1813) major 
```

można uzyskać wpisując w jednym oknie terminala:

```
$ radiusd.rb -h192.168.100.100 -p1813 #uruchomienie serwera major 
```

a w  drugim


```
$ radiusd.rb -minor -p1814 -h192.168.100.101 -H192.168.100.100 -P1813 #uruchomienie serwera minor
```

#### Dołączanie pliku z danymi użytkowników
Domyślnie uruchomiony serwer nie zawiera bazy użytkowników. W celu  dodania bazy należy uruchomić serwer z poleceniem `-f[nazwa_pliku]`, np. aby dołączyć plik *users.dat* należy wykonać polecenie `$ radiusd.rb -fusers.dat` . Pliki z bazą użytkoników to pliki tekstowe, gdzie w każdej linii znajduą się (oddzielone białymi znakami, np. tabulatorami) informacje: nazwa_użytkownika, hasło, prawo_dostępu, np:

plik users.dat

```
adam	haslo	true
radek	haslo	false
iwona	haslo	true
eryk	haslo	true
``` 

### Serwer proxy
Serwer proxy przekierowuje pakiety typu Access-Request do dwóch serwerów protokołu Radius. Odpowiedź serwera proxy uzależniona jest od odpowiedzi z serwerów protokołu Radius, możliwe przypadki to:


|serwer 1			| serwer 2 			| odpowiedź serwera proxy|
|:----------------:|:----------------:	|:---------------:       |
|Access-Accept 		| Access-Accept 	| Access-Accept          |
|Access-Accept 		| timeout 			| Access-Accept          |
|timeout 			| Access-Accept 	| Access-Accept          |
|Access-Accept 		| Access-Reject 	| Access-Reject          |
|Access-Reject 		| Access-Accept 	| Access-Reject          |
|Access-Reject 		| timeout 			| Access-Reject          |
|timeout 			| Access-Reject 	| Access-Reject          |
|timeout 			| timeout 			| Access-Reject          |
|* 					|* 					|timeout                 |

Serwer uruchamia się poleceniem `$ proxyd.rb [OPCJE]` . Dostępne opcje to:

* -h - wyświetla pomoc
* -p[port] ustawia port nasłuchiwnania (domyślnie **1812**)
* -h[host] ustawia adres ip nasluchiwania (domyślnie **127.0.0.1**)
* -P1[port] ustawia port serwera radius1 (domyślnie **1813**)
* -H1[host] ustawia adres ip radius1 (domyślnie **127.0.0.1**)
* -P2[port] ustawia port serwera radius2 (domyślnie **1814**)
* -H2[host] ustawia adres ip radius2 (domyślnie **127.0.0.1**)

#### Ustawienie hostu nasłuchiwania oraz serwerów nadrzędnych
Przykładowa, niestandardowa konfiguracja:

```
proxy (192.168.100.100:1912) <----> (192.168.100.101:1912) serwer Radius 1 (minor lub major)
 					^												|
 					|->	 (192.168.100.102:1912) serwer Radius 2 (minor lub major)
```

Uruchomienie serwera Radius 1

```
$ radiusd.rb -h192.168.100.101 -p1912 
```

Uruchomienie serwera Radius 2

```
$ radiusd.rb -h192.168.100.102 -p1912 
```

Uruchomienie serwera proxy

```ls

$ radiusd.rb -h192.168.100.100 -p1912 -H1192.168.100.101 -P11912 -H2192.168.100.102 -P21912
```

### Klient protokołu Radius
Klient służy do wysyłania pakietów Access-Request do serwerów protokołu Radius. Klient uruchamia się poleceniem `$ rclient.rb [OPCJE]` . Dostępne opcje to:

* -h - wyświetla pomoc
* -h[host] ustawia adres ip procesu (domyślnie **127.0.0.1**)
* -P[port] ustawia port serwera radius (domyślnie **1812**)
* -H[host] ustawia adres ip  serwera radius (domyślnie **127.0.0.1**)

#### Uzyskiwanie połączenia z serwerem Radius

Opcja -h jest niezbędna do prawidłowego wypełnienia atrybutu *NAS-IP-Address* . Przykładowa konfiguracja:

```
client (192.168.100.100) <---> (192.168.100.101:1912) major 
```

Uruchomienie serwera Radius

```
$ radiusd.rb -h192.168.100.101 -p1912
```

Uruchomienie klienta protokołu Radius

```
$ rclient.rb -h192.168.100.100 -H192.168.100.101 -P1912
```

## Scenariusze testów
todo


 





