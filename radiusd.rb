#!/usr/bin/env ruby
require 'rubygems'
require "bundler/setup"

require 'eventmachine'
require 'colorize'
require 'radius/dictionary'
require 'radius/packet'
require 'radius/auth'
require 'digest/sha2'


class RadiusServer < EM::Connection



  def initialize(major_host , major_port, users=[], interactive=false, minor=false,port = '127.0.0.1',secret = Digest::SHA512.digest('pinky').to_s )
    @data = users
    @interactive = interactive
    @minor = minor
    @major_host = major_host
    @major_port = major_port
    @port = port
    @secret = secret
    puts "======================="
    puts "Lista użytkowników"
    puts "LOGIN\tHASŁO\tDOSTĘP"
    @data.each {|k,v|
      print k.to_s + "\t" + v[:pass] + "\t"
      v[:access] ? (puts v[:access].to_s.green) : (puts v[:access].to_s.red)
    }
    puts "======================="

  end

  def receive_data(data)
    dict = Radius::Dictionary.new
    radiusPacket = Radius::Packet.new(dict)

    radiusPacket.unpack(data)


    print "Otrzymano [" +radiusPacket.identifier.to_s+ "]:\t\t"+
              radiusPacket.attr('User-Name')+"\t"+
              radiusPacket.password(@secret )+"\t"

    if @interactive
      print "[tryb interaktywny] naciśnij ENTER aby kongit tynuować..."
      $stdout.flush
      $stdin.gets
    end


    if (@data.has_key? radiusPacket.attr('User-Name').to_sym)
      user = @data[radiusPacket.attr('User-Name').to_sym]
      if (user[:pass] == radiusPacket.password(@secret ).delete("\000") && user[:access] )
        radiusPacket.code = 'Access-Accept'
        puts "OK".green
        radiusPacket.set_attr('Reply-Message','OK')
      else
        radiusPacket.code = 'Access-Reject'
        puts ("brak dostępu (hasło: " + user[:pass] + " dostęp: " + user[:access].to_s + ")").red
        radiusPacket.set_attr('Reply-Message',"brak dostępu (hasło: " + user[:pass] + " dostęp: " + user[:access].to_s + ")")

      end


    else
      if @minor
        print "Delegacja zadania do Major".yellow

        auth = Radius::Auth.new(@major_host+':'+@major_port, @port, 5, @secret, 'dictionaries/dictionary')
        begin
        a = auth.check_passwd(radiusPacket.attr('User-Name'), radiusPacket.password(@secret ), @secret )

        print " otrzymano: ["+ a.identifier.to_s + "] "
        radiusPacket.set_attr('Reply-Message',a.attr('Reply-Message'))
        if a.code == 'Access-Accept'
          radiusPacket.code = 'Access-Accept'
          puts a.attr('Reply-Message').to_s.green
        else
          radiusPacket.code = 'Access-Reject'
          puts a.attr('Reply-Message').to_s.red
        end
        rescue Radius::InvalidAuthenticator
          radiusPacket.code = 'Access-Reject'
          puts ' Invalid Authenticator'.to_s.red
          radiusPacket.set_attr('Reply-Message','major Invalid Authenticator')



        rescue
          radiusPacket.code = 'Access-Reject'
          puts ' major timeout'.to_s.red
          radiusPacket.set_attr('Reply-Message','major timeout')
        end


      else
        radiusPacket.code = 'Access-Reject'
        puts "Użytkownik nie występuje w bazie".red
        radiusPacket.set_attr('Reply-Message',"Użytkownik nie występuje w bazie")

      end


    end


    send_data Radius::Packet.auth_resp(radiusPacket.pack,@secret )
  end
end
users = {}
begin

  interactive = false
  host = '127.0.0.1'
  port = '1812'
  minor = false
  major_port = '1812'
  major_host = '127.0.0.1'
  show_help = false
  secret = Digest::SHA512.digest('pinky').to_s


  ARGV.each do |a|
    interactive = true if a == '-i'
    host = a[2..-1] if a[0, 2] == '-h'
    port = a[2..-1] if a[0, 2] == '-p'
    major_port = a[2..-1] if a[0, 2] == '-P'
    major_host = a[2..-1] if a[0, 2] == '-H'
    minor = true if a == '-minor'
    secret = a[2..-1] if a[0, 2] == '-s'
    if a == '-h'
      show_help = true
      puts "
          Serwer protokołu RADIUS. Opcje:
            -h - wyświetla pomoc
            -p[port] ustawia port nasłuchiwnania
            -h[host] ustawia adres ip nasluchiwania
            -minor - serwer pracuje w trybie podrzednym
            -P[port] ustawia port serwera nadrzędnego (aktywne z flagą -minor)
            -H[host] ustawia adres ip serwera nadrzednego (aktywne z flagą -minor)
            -f[plik] ładuje baze użytkowników z pliku
            -i praca w trybie interaktywnym (odpowiedz po nacisnieciu ENTER)
            -s[sekret] ustawia secret
          "

    end

    if a[0,2] == '-f'
      i = 0
      File.open(a[2..-1], "r").each_line do |line|
        i+=1
        begin
        user = line.split
        users[user[0].to_sym]={:pass => user[1], :access => user[2]=='true' }
        rescue
          puts 'Błąd w pliku z danymi użytkowników, linia: '.red   + i.to_s.red
        end
      end

    end



  end
  unless show_help
    EM.run do
        EventMachine.error_handler { |e|
        puts "Error raised during event loop: #{e}"
      }

      puts 'Włączony tryb interaktywny  ' if interactive
      minor ? puts('Serwer podrzędny') : puts('Serwer nadrzędny')
      puts 'host: ' + host
      puts 'port: ' + port
      puts 'host serwera nadrzędnego: ' + major_host if minor
      puts 'port serwera nadrzędnego: ' + major_port if minor
      puts 'Sekret: ' + secret

      EM.epoll

      EM.open_datagram_socket host, port, RadiusServer,major_host,major_port, users, interactive, minor, port, secret do |connection|
      end

    end


  end

rescue Exception => e
  puts 'Blad: ' + e if e.to_s.length > 0

end