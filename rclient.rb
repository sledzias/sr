#!/usr/bin/env ruby
require 'rubygems'
require "bundler/setup"

require 'radius/auth'
require 'timeout'
require 'colorize'
require 'digest/sha2'


radius_host  = '127.0.0.1'
radius_port ='1812'
host = '127.0.0.1'
show_help = false
secret = Digest::SHA512.digest('pinky').to_s

user = ''
pass = ''


ARGV.each do |a|
  host = a[2..-1] if a[0, 2] == '-h'
  radius_port = a[2..-1] if a[0, 2] == '-P'
  radius_host = a[2..-1] if a[0, 2] == '-H'
  user = a[2..-1] if a[0, 2] == '-u'
  pass = a[2..-1] if a[0, 2] == '-p'
  secret = a[2..-1] if a[0, 2] == '-s'

  if a == '-h'
    show_help = true
    puts "
          Klient protokołu RADIUS. Opcje:
            -h - wyświetla pomoc
            -h[host] ustawia adres ip procesu
            -P[port] ustawia port serwera radius
            -H[host] ustawia adres ip radius
            -u[nazwa] nazwa użytkownika
            -p[haslo] hasło użytkownika
            -s[sekret] ustawia secret
          "

  end
end

unless show_help
auth = Radius::Auth.new(radius_host+":"+radius_port, host, 25, secret,'dictionaries/dictionary')

puts "radius host: " + radius_host+":"+radius_port
puts 'Sekret: ' + secret

if (user.empty? && pass.empty?)
  print "login: "
  user = $stdin.readline
  user.chomp!
  print "Password: "
  pass = $stdin.readline
  pass.chomp!
else
 puts 'login: ' + user.to_s
 puts 'Password: ' + pass.to_s
end

begin
  begin


    a=auth.check_passwd(user, pass, secret)

    if (a.code == 'Access-Accept')
      print "Odpowiedź: ["+ a.identifier.to_s + "] 'Access-Accept' "
      puts a.attr('Reply-Message').to_s.green

    else
      print "Odpowiedź: ["+ a.identifier.to_s + "] 'Access-Reject' "
      puts a.attr('Reply-Message').to_s.red

    end



  rescue Radius::InvalidAuthenticator

     puts 'Invalid Authenticator'

  rescue Exception
    puts 'timed out'
  end
  end
end