# Serwer Radius-alike
## Temat projektu
"Zrealizować system uwierzytelnienia użytkowników w oparciu o grupę serwerów protokołu RADIUS działających w hierarchii zaufania:
serwer nadrzędny + 2 serwery podrzędne.
Serwer podrzędny kieruje zapytanie do serwera nadrzędnego jeśli dany użytkownik nie występuje w lokalnej bazie danych.
Baza danych musi zawierać wpisy pozytywne i negatywne (uniemożliwiające uwierzytelnienie).
Dodatkowo zrealizować serwer proxy kierujący zapytania jednocześnie do 2 serwerów.
Wynik uwierzytelnienia w takim przypadku jest najgorszym wynikiem uwierzytelnienia z dwóch odpowiedzi."
## Opis projektu
Projekt zrealizowany w ramach przedmiotu Systemy Rozproszone. W implementacji wykorzystałem bibliotekę [eventmachine](http://rubyeventmachine.com) do zrealizowania serwera oraz bibliotekę [ruby-radius](https://github.com/nearbuy/ruby-radius) udostępniającą wrappery języka ruby na pakiety Radiusa. Implementacja spełnia wymagania opisane w dokumencie [RADIUS-alike protocol specification](https://docs.google.com/document/d/1GW-s8L0PKphUtIzk0JnOE5krszIxjigc308Fdnlp0Dg/edit#heading=h.jlii8ngyn8m5).  

###Wymagania

* ruby 1.8.7
* [Bundler](http://gembundler.com "Bundler")  

###Instalacja
Należy przejść do folderu zawierającego pliki projektu i wykonać polecenie:

```
$ bundle install
```
## Opis użycia
###Serwer protokołu Radius
Serwer służy do odpowiadania na pakiety protokołu Radius zgodnie z opisem w dokumencie [RADIUS-alike protocol specification](https://docs.google.com/document/d/1GW-s8L0PKphUtIzk0JnOE5krszIxjigc308Fdnlp0Dg/edit#heading=h.vkgolmot2phs). Serwer może pracować w trybie nadrzędnym (major) oraz podrzędnym (minor). W trybie minor serwer kieruje zapytania do serwera major, jeżeli nie potrafi sam odpowiedzieć na pakiet typu Access-Request (użytkownik wymieniony w pakiecie nie znajduje się w bazie danych serwera minor).
Serwer uruchamia się poleceniem `$ radiusd.rb [OPCJE]` . Dostępne opcje to:

* -h - wyświetlenie pomocy
* -p[port] ustawia port nasłuchiwnania serwera (domyślnie **127.0.0.1**)
* -h[host] ustawia adres ip nasluchiwania serwera (domyślnie **1812**)
* -minor - serwer pracuje w trybie podrzędnym (domyślnie serwer pracuje jako major)
* -P[port] ustawia port serwera nadrzędnego (aktywne z flagą -minor), domyślnie **1812**
* -H[host] ustawia adres ip serwera nadrzednego (aktywne z flagą -minor), domyślnie **127.0.0.1**
* -f[plik] ładuje baze użytkowników z pliku (domyślnie baza użytkowników jest pusta)
* -i praca w trybie interaktywnym (odpowiedz po nacisnieciu ENTER)
* -s[sekret] ustawia secret (domyślnie SHA512('pinky') )

#### Ustawienie hostu i portu nasłuchiwania
Opcja ta przydaje się, gdy potrzebna jest zmiana domyślnej konfiguracji. Przykłodowo, aby zmienić host na 192.167.100.101:1813 należy uruchomić serwer następującym poleceniem:

```
$ radius.rb -h192.167.100.101 -p1813
```
#### Ustawienie pracy serwera w trybie minor
W tym trybie serwer deleguje zapytanie do nadrzędnego serwera, jeżeli w zapytaniu znajduje się zapytanie o użytkownika, którego serwer nie ma w bazie danych. 
Konfigurację:

```
minor (127.0.0.1:1813) <---> (127.0.0.1:1812) major 
```

można uzyskać wpisując w jednym oknie terminala (**ważna jest kolejność uruchamiania serwerów**):

```
$ radiusd.rb #uruchomienie serwera major 
```

a w  drugim


```
$ radiusd.rb -minor -p1813 #uruchomienie serwera minor
```

Oczywiście można zmienić domyślne hosty/porty dla serwera minor/major, np:

```
minor (192.168.100.101:1814) <---> (192.168.100.100:1813) major 
```

można uzyskać wpisując w jednym oknie terminala:

```
$ radiusd.rb -h192.168.100.100 -p1813 #uruchomienie serwera major 
```

a w  drugim


```
$ radiusd.rb -minor -p1814 -h192.168.100.101 -H192.168.100.100 -P1813 #uruchomienie serwera minor
```

#### Dołączanie pliku z danymi użytkowników
Domyślnie uruchomiony serwer nie zawiera bazy użytkowników. W celu  dodania bazy należy uruchomić serwer z poleceniem `-f[nazwa_pliku]`, np. aby dołączyć plik *users.dat* należy wykonać polecenie `$ radiusd.rb -fusers.dat` . Pliki z bazą użytkoników to pliki tekstowe, gdzie w każdej linii znajduą się (oddzielone białymi znakami, np. tabulatorami) informacje: nazwa_użytkownika, hasło, prawo_dostępu, np:

plik users.dat

```
adam	haslo	true
radek	haslo	false
iwona	haslo	true
eryk	haslo	true
``` 

### Serwer proxy
Serwer proxy przekierowuje pakiety typu Access-Request do dwóch serwerów protokołu Radius. Odpowiedź serwera proxy uzależniona jest od odpowiedzi z serwerów protokołu Radius, możliwe przypadki to:


|serwer 1			| serwer 2 			| odpowiedź serwera proxy|
|:----------------:|:----------------:	|:---------------:       |
|Access-Accept 		| Access-Accept 	| Access-Accept          |
|Access-Accept 		| timeout 			| Access-Accept          |
|timeout 			| Access-Accept 	| Access-Accept          |
|Access-Accept 		| Access-Reject 	| Access-Reject          |
|Access-Reject 		| Access-Accept 	| Access-Reject          |
|Access-Reject 		| timeout 			| Access-Reject          |
|timeout 			| Access-Reject 	| Access-Reject          |
|timeout 			| timeout 			| Access-Reject          |
|* 					|* 					|timeout                 |

Serwer uruchamia się poleceniem `$ proxyd.rb [OPCJE]` . Dostępne opcje to:

* -h - wyświetla pomoc
* -p[port] ustawia port nasłuchiwnania (domyślnie **1812**)
* -h[host] ustawia adres ip nasluchiwania (domyślnie **127.0.0.1**)
* -P1[port] ustawia port serwera radius1 (domyślnie **1813**)
* -H1[host] ustawia adres ip radius1 (domyślnie **127.0.0.1**)
* -P2[port] ustawia port serwera radius2 (domyślnie **1814**)
* -H2[host] ustawia adres ip radius2 (domyślnie **127.0.0.1**)
* -s[sekret] ustawia secret (domyślnie SHA512('pinky') )

#### Ustawienie hostu nasłuchiwania oraz serwerów nadrzędnych
Przykładowa, niestandardowa konfiguracja:

```
proxy (192.168.100.100:1912) <----> (192.168.100.101:1912) serwer Radius 1 (minor lub major)
 					^												|
 					|->	 (192.168.100.102:1912) serwer Radius 2 (minor lub major)
```

Uruchomienie serwera Radius 1

```
$ radiusd.rb -h192.168.100.101 -p1912 
```

Uruchomienie serwera Radius 2

```
$ radiusd.rb -h192.168.100.102 -p1912 
```

Uruchomienie serwera proxy

```ls

$ radiusd.rb -h192.168.100.100 -p1912 -H1192.168.100.101 -P11912 -H2192.168.100.102 -P21912
```

### Klient protokołu Radius
Klient służy do wysyłania pakietów Access-Request do serwerów protokołu Radius. Klient uruchamia się poleceniem `$ rclient.rb [OPCJE]` . Dostępne opcje to:

* -h - wyświetla pomoc
* -h[host] ustawia adres ip procesu (domyślnie **127.0.0.1**)
* -P[port] ustawia port serwera radius (domyślnie **1812**)
* -H[host] ustawia adres ip  serwera radius (domyślnie **127.0.0.1**)
* -u[nazwa] nazwa użytkownika (domyślnie )
* -p[haslo] hasło użytkownika
* -s[sekret] ustawia secret (domyślnie SHA512('pinky') )

Klient działa jedynie w trybie interaktywnym


#### Uzyskiwanie połączenia z serwerem Radius

Opcja -h jest niezbędna do prawidłowego wypełnienia atrybutu *NAS-IP-Address* . Przykładowa konfiguracja:

```
client (192.168.100.100) <---> (192.168.100.101:1912) major 
```

Uruchomienie serwera Radius

```
$ radiusd.rb -h192.168.100.101 -p1912
```

Uruchomienie klienta protokołu Radius

```
$ rclient.rb -h192.168.100.100 -H192.168.100.101 -P1912
```

## Scenariusze testów
### Komunikacja klient - serwer nadrzędny

Konfiguracja:

```
client (127.0.0.1) <---> (127.0.0.1:1812) major 
```
Baza danych serwera:

```
Lista użytkowników
LOGIN	HASŁO	DOSTĘP
adam	haslo	true
radek	haslo	false
iwona	haslo	true
eryk	haslo	true

```
Opis: Wysłanie zapytania z prawidłową nazwą i hasłem użytkownika, który ma dostęp do zasobu.
Przebieg testów:


1.	 Uruchomienie serwera poleceniem `$ ./radiusd.rb -fusers.dat`
2.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -uadam -phaslo`

Oczekiwany wynik:
Serwer zwraca odpowiedź 'Access-Accept'

Opis: Wysłanie zapytania z prawidłową nazwą i złym hasłem użytkownika 


Przebieg testów:


1.	 Uruchomienie serwera poleceniem `$ ./radiusd.rb -fusers.dat`
2.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -uadam -pzlehaslo`

Oczekiwany wynik:
Serwer zwraca odpowiedź 'Access-Reject' brak dostępu (hasło: haslo dostęp: true)

Opis: Wysłanie zapytania z prawidłową nazwą i hasłem użytkownika znajdującego się na czarnej liście 


Przebieg testów:


1.	 Uruchomienie serwera poleceniem `$ ./radiusd.rb -fusers.dat`
2.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -uradek -phaslo`

Oczekiwany wynik:
Serwer zwraca odpowiedź 'Access-Reject' brak dostępu (hasło: haslo dostęp: false)

Opis: Wysłanie zapytania z nazwą użytkownika nieznajdującego się w bazie 


Przebieg testów:


1.	 Uruchomienie serwera poleceniem `$ ./radiusd.rb -fusers.dat`
2.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -unieznany -phaslo`

Oczekiwany wynik:
Serwer zwraca odpowiedź 'Access-Reject' Użytkownik nie występuje w bazie

Opis: Wysłanie zapytania z prawidłową nazwą i hasłem użytkownika, który ma dostęp do zasobu. Serwer ma inny sekret niż klient
Przebieg testów:


1.	 Uruchomienie serwera poleceniem `$ ./radiusd.rb -sSEKRET -fusers.dat`
2.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -uadam -phaslo`

Oczekiwany wynik:
Klient dostaje wiadomość z błędnym polem authenticator i obsługuje wyjątek: Invalid Authenticator.

### Komunikacja klient - serwer podrzędny - serwer nadrzędny

Konfiguracja:

```
client (127.0.0.1) <---> (127.0.0.1:1812) major 
```
Baza danych serwera nadrzędnego (major):

```
Lista użytkowników
LOGIN	HASŁO	DOSTĘP
adam	haslo	true
radek	haslo	false
iwona	haslo	true
eryk	haslo	true
```

Baza danych serwera podrzędnego (minor)

```
Lista użytkowników
LOGIN	HASŁO	DOSTĘP
adam	haslo	false
radek	haslo	false
iwona	haslo	true
```
Opis: Wysłanie zapytania z prawidłową nazwą i hasłem użytkownika, który ma dostęp do zasobu. Użytkownik znajduje się w bazie danych serwera podrzędnego.

Przebieg testów:

1.	 Uruchomienie serwera nadrzędnego poleceniem  `$ ./radiusd.rb -fusers.dat -p1813`
2.	 Uruchomienie serwera podrzędnego poleceniem `$ ./radiusd.rb -minor -P1813 -fusers-minor1.dat`
3.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -uadam -phaslo`

Oczekiwany wynik:
Serwer podrzędny korzysta ze swojej bazy i zwraca odpowiedź 'Access-Accept'

Opis: Wysłanie zapytania z prawidłową nazwą i prawidłowym hasłem użytkownika. Użytkownik znajduje się w bazie danych serwera nadrzędnego i nie znajduje się w bazie danych serwera podrzędnego.

Przebieg testów:

1.	 Uruchomienie serwera nadrzędnego poleceniem  `$ ./radiusd.rb -fusers.dat -p1813`
2.	 Uruchomienie serwera podrzędnego poleceniem `$ ./radiusd.rb -minor -P1813 -fusers-minor1.dat`
3.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -ueryk -phaslo` 

Oczekiwany wynik:
Serwer podrzędny deleguje zapytanie do serwera nadrzędnego, serwer nadrzędny zwraca odpowiedź 'Access-Accept' i serwer podrzędny przekazuje ją do klienta.

Opis: Przerwanie połączenia serwer podrzędny - serwer nadrzędny

Przebieg testów:

1.	 Uruchomienie serwera nadrzędnego poleceniem  `$ ./radiusd.rb -i -fusers.dat -p1813`
2.	 Uruchomienie serwera podrzędnego poleceniem `$ ./radiusd.rb  -minor -P1813 -fusers-minor1.dat`
3.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -ueryk -phaslo`

Oczekiwany wynik:
Klient otrzymuje odpowiedź 'Access-Reject' major timeout  

Opis serwer nadrzędny ma inny secret niż klient i serwer podrzędny

Przebieg testów:

1.	 Uruchomienie serwera nadrzędnego poleceniem  `$ ./radiusd.rb -i -fusers.dat -p1813 -sSECRET`
2.	 Uruchomienie serwera podrzędnego poleceniem `$ ./radiusd.rb  -minor -P1813 -fusers-minor1.dat`
3.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -ueryk -phaslo`

Oczekiwany wynik:
Klient otrzymuje odpowiedź: 'Access-Reject' major Invalid Authenticator

### Komunikacja klient - serwer proxy- serwer podrzędny (x2) - serwer nadrzędny

Konfiguracja:

```
klient (l) <-> proxy (l:1912) <-> (l:1913) s1 <->  (l:1915) major            
 			                   ^		        ^                                                                                                    
 					           |->(l:1914) s2 -/
```
Baza danych serwera nadrzędnego (major):

```
Lista użytkowników
LOGIN	HASŁO	DOSTĘP
adam	haslo	true
radek	haslo	false
iwona	haslo	true
eryk	haslo	true
```

Baza danych serwera podrzędnego (s1)

```
Lista użytkowników
adam	haslo	false
radek	haslo	false
iwona	haslo	true
```

Baza danych serwera podrzędnego (s2)

```
Lista użytkowników
LOGIN	HASŁO	DOSTĘP
adam	haslo	true
radek	haslo	false
iwona	haslo	true
eryk	haslo	true
```


Opis: Wysłanie zapytania z nazwą i hasłem użytkownika. Użytkownik występuje w bazie danych s1 (zablokowany dostęp) i s2 (dostęp)

Przebieg testów:

1.	 Uruchomienie serwera nadrzędnego poleceniem  `$ ./radiusd.rb  -fusers.dat -p1815`
2.	 Uruchomienie serwera podrzędnego (s1) poleceniem `$ ./radiusd.rb -minor -P1815 -p1813 -fusers-minor1.dat`
3.	 Uruchomienie serwera podrzędnego (s2) poleceniem `$ ./radiusd.rb -minor -P1815 -p1814 -fusers-minor2.dat`
4.	 Uruchomienie serwera proxy poleceniem `$ ./proxyd.rb -P11813 -P21814`
4.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -uadam -phaslo`

Oczekiwany wynik:
Klient otrzymuje odpowiedź 'Access-Reject'  r1: brak dostępu (hasło: haslo dostęp: false) r2: OK , ponieważ użytkownik adam nie ma dostępu w s1 oraz ma dostęp w s2

Opis: Wysłanie zapytania z nazwą i hasłem użytkownika. Użytkownik nie występuje w bazie danych s1, występuje w  s2 (dostęp) i serwerze nadrzędnym (dostęp)

Przebieg testów:

1.	 Uruchomienie serwera nadrzędnego poleceniem  `$ ./radiusd.rb  -fusers.dat -p1815`
2.	 Uruchomienie serwera podrzędnego (s1) poleceniem `$ ./radiusd.rb -minor -P1815 -p1813 -fusers-minor1.dat`
3.	 Uruchomienie serwera podrzędnego (s2) poleceniem `$ ./radiusd.rb -minor -P1815 -p1814 -fusers-minor2.dat`
4.	 Uruchomienie serwera proxy poleceniem `$ ./proxyd.rb -P11813 -P21814`
4.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -ueryk -phaslo`

Oczekiwany wynik:
S1 przekierowuje zapytanie do serwera nadrzędnego. Klient otrzymuje odpowiedź 'Access-Accept'  r1: OK r2: OK

Opis: Wysłanie zapytania z nazwą i hasłem użytkownika. Użytkownik nie występuje w bazie danych s1, występuje w  s2 (dostęp) i serwerze nadrzędnym (dostęp). Przerwanie połączenia z serwerem nadrzędnym

Przebieg testów:

1.	 Uruchomienie serwera nadrzędnego poleceniem  `$ ./radiusd.rb -i -fusers.dat -p1815`
2.	 Uruchomienie serwera podrzędnego (s1) poleceniem `$ ./radiusd.rb -minor -P1815 -p1813 -fusers-minor1.dat`
3.	 Uruchomienie serwera podrzędnego (s2) poleceniem `$ ./radiusd.rb -minor -P1815 -p1814 -fusers-minor2.dat`
4.	 Uruchomienie serwera proxy poleceniem `$ ./proxyd.rb -P11813 -P21814`
4.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -ueryk -phaslo`

Oczekiwany wynik:
S1 przekierowuje zapytanie do serwera nadrzędnego. Serwer nadrzędny nie odpowiada (timeout) Klient otrzymuje odpowiedź 'Access-Reject'  r1: major timeout r2: OK . Komentarz: Access-Reject dlatego, ponieważ timeout wystąpił na serwerze nadrzędnym.


Opis: Wysłanie zapytania z nazwą i hasłem użytkownika. Użytkownik  występuje w bazie danych s1 (brak dostępu),   s2 (dostęp). Przerwanie połączenia z serwerem s1

Przebieg testów:

1.	 Uruchomienie serwera nadrzędnego poleceniem  `$ ./radiusd.rb  -fusers.dat -p1815`
2.	 Uruchomienie serwera podrzędnego (s1) poleceniem `$ ./radiusd.rb -i -minor -P1815 -p1813 -fusers-minor1.dat`
3.	 Uruchomienie serwera podrzędnego (s2) poleceniem `$ ./radiusd.rb -minor -P1815 -p1814 -fusers-minor2.dat`
4.	 Uruchomienie serwera proxy poleceniem `$ ./proxyd.rb -P11813 -P21814`
4.	 Uruchomienie klienta poleceniem `$ ./rclient.rb -uadam -phaslo`

Oczekiwany wynik:
S1 nie zwraca odpowiedzi. Serwer proxy głosuje: timeout serwera podrzędnego i 'Access-Accept' daje 'Access-Accept' r1: timeout  r2: OK.



 



 





