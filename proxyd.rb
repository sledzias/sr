#!/usr/bin/env ruby
require 'rubygems'
require "bundler/setup"

require 'eventmachine'
require 'colorize'
require 'radius/dictionary'
require 'radius/packet'
require 'radius/auth'
require 'digest/sha2'


class RadiusServer < EM::Connection



  def initialize(host1 , port1,host2 , port2,port = '127.0.0.1', secret = Digest::SHA512.digest('pinky').to_s)
    @host1 = host1
    @port1 = port1
    @host2 = host2
    @port2 = port2
    @port  = port
    @secret = secret
  end

  def receive_data(data)
    dict = Radius::Dictionary.new
    radiusPacket = Radius::Packet.new(dict)
    radiusPacket.unpack(data)

    $stdout.flush

    puts "Otrzymano [" +radiusPacket.identifier.to_s+ "]:\t\t"+radiusPacket.attr('User-Name')+"\t"+radiusPacket.password(@secret)+"\tprzekazanie zapytania do r1 i r2"
    $stdout.flush
    auth1 = Radius::Auth.new(@host1+':'+@port1, @port, 10,@secret,'dictionaries/dictionary')
    auth2 = Radius::Auth.new(@host2+':'+@port2, @port, 10,@secret, 'dictionaries/dictionary')

    begin
      r1 = auth1.check_passwd(radiusPacket.attr('User-Name'), radiusPacket.password(@secret), @secret)
    rescue
      r1 = nil
    end

    begin
      r2 = auth2.check_passwd(radiusPacket.attr('User-Name'), radiusPacket.password(@secret), @secret)
    rescue
      r2 = nil
    end

    if !r1.nil?
      if r1.code == 'Access-Accept'
        puts "r1: ["+ r1.identifier.to_s + "]\t"+r1.attr('Reply-Message').to_s.green
      else
        puts "r1: ["+ r1.identifier.to_s + "]\t"+r1.attr('Reply-Message').to_s.red
      end
    else
      puts "r1:\t" + 'timeout'.red
    end

    if !r2.nil?
      if r2.code == 'Access-Accept'
        puts "r2: ["+ r2.identifier.to_s + "]\t"+r2.attr('Reply-Message').to_s.green
      else
        puts "r2: ["+ r2.identifier.to_s + "]\t"+r2.attr('Reply-Message').to_s.red
      end
    else
      puts "r2:\t" + 'timeout'.red
    end

    message = ''
    r1.nil? ? ( message += "r1: timeout ") :(  message += " r1: " + r1.attr('Reply-Message').to_s  )
    r2.nil? ? ( message += "r2: timeout ") :  ( message += " r2: " +  r2.attr('Reply-Message').to_s  )
    radiusPacket.set_attr('Reply-Message',message)
    print "Zwracam odpowiedź [" + radiusPacket.identifier.to_s + "]: "
    $stdout.flush
      if  (r1.nil? || r1.code == 'Access-Accept') &&  (r2.nil? || r2.code == 'Access-Accept') && !(r1.nil? && r2.nil?)
        radiusPacket.code = 'Access-Accept'
        puts message.to_s.green
      else
        radiusPacket.code = 'Access-Reject'
        puts message.to_s.red
      end


    send_data Radius::Packet.auth_resp(radiusPacket.pack,@secret)
  end
end
users = {}
begin
  host1 = '127.0.0.1'
  port1 ='1813'
  host2  = '127.0.0.1'
  port2 ='1814'
  host = '127.0.0.1'
  port = '1812'
  show_help = false
  secret = Digest::SHA512.digest('pinky').to_s



  ARGV.each do |a|
    host = a[2..-1] if a[0, 2] == '-h'
    port = a[2..-1] if a[0, 2] == '-p'
    port1 = a[2..-1] if a[0, 2] == '-P1'
    host1 = a[2..-1] if a[0, 2] == '-H1'
    port2 = a[2..-1] if a[0, 2] == '-P2'
    host2 = a[2..-1] if a[0, 2] == '-H2'
    secret = a[2..-1] if a[0, 2] == '-s'
    if a == '-h'
      show_help = true
      puts "
          Serwer protokołu RADIUS. Opcje:
            -h - wyświetla pomoc
            -p[port] ustawia port nasłuchiwnania
            -h[host] ustawia adres ip nasluchiwania
            -P1[port] ustawia port serwera radius1
            -H1[host] ustawia adres ip radius1
            -P2[port] ustawia port serwera radius2
            -H2[host] ustawia adres ip radius2
            -s[sekret] ustawia secret
          "

    end
  end
  unless show_help
    EM.run do
      EventMachine.error_handler { |e|
        puts "Error raised during event loop: #{e}"
      }

      puts 'host: ' + host
      puts 'port: ' + port
      puts 'host radius1 [r1]: ' + host1 +":" + port1
      puts 'host radius2 [r2]: ' + host2 +":" + port2
      puts 'Sekret: ' + secret

      EM.epoll

      EM.open_datagram_socket host, port, RadiusServer,host1 , port1,host2 , port2, port, secret do |connection|
      end

    end


  end

rescue Exception => e
  puts 'Blad: ' + e if e.to_s.length > 0

end